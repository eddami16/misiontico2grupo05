function loadData(){
    let request = sendRequest('cita/list', 'GET', '')
    let table = document.getElementById('appointment-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_Cita}</th>
                    <td>${element.fecha_Cita}</td>
                    <td>${element.hora_Cita}</td>
                    <td>${element.consultorio}</td>
                    <td>${element.id_Doctor}</td>
                    <td>${element.nombre_Doctor}</td>
                    <td>${element.id_Paciente}</td>
                    <td>${element.reserva}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_citas.html?id=${element.id_Cita}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteCita(${element.id_Cita})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="9">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadCita(id_Cita){
    let request = sendRequest('cita/list/'+id_Cita, 'GET', '')
    let id = document.getElementById('appointment-id')
    let fecha = document.getElementById('appointment-fecha')
    let hora = document.getElementById('appointment-hora')
    let consultorio = document.getElementById('appointment-consultorio')
    let idDoctor = document.getElementById('appointment-doctor')
    let nombreDoctor = document.getElementById('appointment-namedoctor')
    let idPaciente = document.getElementById('appointment-id_Paciente')
    let reserva = document.getElementById('appointment-reserva')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_Cita
        fecha.value = data.fecha_Cita
        hora.value = data.hora_Cita
        consultorio.value = data.consultorio
        idDoctor.value = data.id_Doctor
        nombreDoctor.value = data.nombre_Doctor
        idPaciente.value = data.id_Paciente
        reserva.value = data.reserva
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteCita(id_Cita){
    let request = sendRequest('cita/'+id_Cita, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}
/*
function editarCita(){
    let id = document.getElementById('appointment-id').value
    let fecha = document.getElementById('appointment-fecha').value
    let hora = document.getElementById('appointment-hora').value
    let consultorio = document.getElementById('appointment-consultorio').value
    let idDoctor = document.getElementById('appointment-doctor').value
    let nombreDoctor = document.getElementById('appointment-namedoctor').value
    let idPaciente = document.getElementById('appointment-id_Paciente').value
    let reserva = document.getElementById('appointment-reserva').value
    
    let data = {'id_Cita': id, 'fecha_Cita': fecha, 'hora_Cita': hora, 'consultorio': consultorio,'id_Doctor': idDoctor, 'nombre_Doctor': nombreDoctor, 'id_Paciente': idPaciente, 'reserva': reserva}
    let request = sendRequest('cita/', id ? 'PUT' : data)
    request.onload = function(){
        alert('Se guardaron los cambios.')
        window.location = 'citas.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }

    
}
*/
function guardarCita(){
    let id = document.getElementById('appointment-id').value
    let fecha = document.getElementById('appointment-fecha').value
    let hora = document.getElementById('appointment-hora').value
    let consultorio = document.getElementById('appointment-consultorio').value
    let idDoctor = document.getElementById('appointment-doctor').value
    let nombreDoctor = document.getElementById('appointment-namedoctor').value
    let idPaciente = document.getElementById('appointment-id_Paciente').value
    let reserva = document.getElementById('appointment-reserva').value
    
    let data = {'id_Cita': id, 'fecha_Cita': fecha, 'hora_Cita': hora, 'consultorio': consultorio,'id_Doctor': idDoctor, 'nombre_Doctor': nombreDoctor, 'id_Paciente': idPaciente, 'reserva': reserva}
    let request = sendRequest('cita/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        alert('Se guardaron los cambios.')
        window.location = 'citas.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }

    
}