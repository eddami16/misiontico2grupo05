
function loadData(){
    let request = sendRequest('paciente/list', 'GET', '')
    let table = document.getElementById('users-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_Paciente}</th>
                    <td>${element.tipo_Identificacion}</td>
                    <td>${element.nombre}</td>
                    <td>${element.apellidos}</td>
                    <td>${element.telefono}</td>
                    <td>${element.eapb}</td>
                    <td>${element.email}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_pacientes.html?id=${element.id_Paciente}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.id_Paciente})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="8">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadUser(id_Paciente){
    let request = sendRequest('paciente/list/'+id_Paciente, 'GET', '')
    let id = document.getElementById('user-id')
    let tipo = document.getElementById('user-tipo')
    let nombre = document.getElementById('user-nombre')
    let apellido = document.getElementById('user-apellido')
    let telefono = document.getElementById('user-telefono')
    let eapb = document.getElementById('user-eapb')
    let email = document.getElementById('user-email')
    request.onload = function(){
        
        let data = request.response;
        id.value = data.id_Paciente
        tipo.value = data.tipo_Identificacion
        nombre.value = data.nombre
        apellido.value = data.apellidos
        telefono.value = data.telefono
        eapb.value = data.eapb
        email.value = data.email
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteUser(id_Paciente){
    let request = sendRequest('paciente/'+id_Paciente, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUser(){
    let id = document.getElementById('user-id').value
    let tipo = document.getElementById('user-tipo').value
    let nombre = document.getElementById('user-nombre').value
    let apellido = document.getElementById('user-apellido').value
    let telefono = document.getElementById('user-telefono').value
    let eapb = document.getElementById('user-eapb').value
    let email = document.getElementById('user-email').value
    
    let data = {'id_Paciente': id, 'tipo_Identificacion': tipo, 'nombre': nombre, 'apellidos': apellido,'telefono': telefono, 'eapb': eapb, 'email': email}
    let request = sendRequest('paciente/', id ? 'PUT' : data)
    request.onload = function(){
        alert('Se guardaron los cambios.')
        window.location = 'pacientes.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }

    
}

function saveUserG(){
    let id = document.getElementById('user-id').value
    let tipo = document.getElementById('user-tipo').value
    let nombre = document.getElementById('user-nombre').value
    let apellido = document.getElementById('user-apellido').value
    let telefono = document.getElementById('user-telefono').value
    let eapb = document.getElementById('user-eapb').value
    let email = document.getElementById('user-email').value
    
    let data = {'id_Paciente': id, 'tipo_Identificacion': tipo, 'nombre': nombre, 'apellidos': apellido,'telefono': telefono, 'eapb': eapb, 'email': email}
    let request = sendRequest('paciente/', 'POST' , data)
    request.onload = function(){
        alert('Se guardaron los cambios.')
        window.location = 'pacientes.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }

    
}