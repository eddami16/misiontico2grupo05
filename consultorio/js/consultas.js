
function loadData(){
    let request = sendRequest('consulta/list', 'GET', '')
    let table = document.getElementById('consult-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_Consulta}</th>
                    <td>${element.fecha_Cons}</td>
                    <td>${element.hora_Cons}</td>
                    <td>${element.id_Cita}</td>
                    <td>${element.consulta}</td>
                    <td>${element.nombre_Med}</td>
                    <td>${element.cant_Med}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_consultas.html?id=${element.id_Consulta}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteConsulta(${element.id_Consulta})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="8">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadConsulta(id_Consulta){
    let request = sendRequest('consulta/list/'+id_Consulta, 'GET', '')
    let id = document.getElementById('consult-id')
    let fecha = document.getElementById('consult-date')
    let hora = document.getElementById('consult-datetime')
    let idCita = document.getElementById('consult-idcita')
    let consulta = document.getElementById('consult-consulta')
    let Medicamento = document.getElementById('consult-namemedicine')
    let cantidadMed = document.getElementById('consult-quantitymedicine')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_Consulta
        fecha.value = data.fecha_Cons
        hora.value = data.hora_Cons
        idCita.value = data.id_Cita
        consulta.value = data.consulta
        Medicamento.value = data.nombre_Med
        cantidadMed.value = data.cant_Med
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteConsulta(id_Consulta){
    let request = sendRequest('consulta/'+id_Consulta, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveConsulta(){
    let id = document.getElementById('consult-id').value
    let fecha = document.getElementById('consult-date').value
    let hora = document.getElementById('consult-datetime').value
    let idCita = document.getElementById('consult-idcita').value
    let consulta = document.getElementById('consult-consulta').value
    let Medicamento = document.getElementById('consult-namemedicine').value
    let cantidadMed = document.getElementById('consult-quantitymedicine').value
    let data = {'id_Consulta': id,'fecha_Cons':fecha,'hora_Cons': hora, 'id_Cita': idCita, 'consulta': consulta , 'nombre_Med':  Medicamento , 'cant_Med': cantidadMed}
    let request = sendRequest('consulta/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'consultas.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}