-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-09-2022 a las 07:18:39
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `clinica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `id_Cita` int(11) NOT NULL,
  `fecha_Cita` date NOT NULL,
  `hora_Cita` datetime NOT NULL,
  `consultorio` varchar(10) NOT NULL,
  `id_Doctor` int(11) NOT NULL,
  `nombre_Doctor` varchar(45) NOT NULL,
  `id_Paciente` int(11) NOT NULL,
  `reserva` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`id_Cita`, `fecha_Cita`, `hora_Cita`, `consultorio`, `id_Doctor`, `nombre_Doctor`, `id_Paciente`, `reserva`) VALUES
(1, '2022-09-16', '2022-09-16 00:08:00', '01', 123, 'Medico de Prueba', 244354, 'Si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consulta`
--

CREATE TABLE `consulta` (
  `id_Consulta` int(11) NOT NULL,
  `fecha_Cons` date NOT NULL,
  `hora_Cons` datetime NOT NULL,
  `id_Cita` int(11) NOT NULL,
  `consulta` varchar(215) NOT NULL,
  `nombre_Med` varchar(45) NOT NULL,
  `cant_Med` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `consulta`
--

INSERT INTO `consulta` (`id_Consulta`, `fecha_Cons`, `hora_Cons`, `id_Cita`, `consulta`, `nombre_Med`, `cant_Med`) VALUES
(1, '2022-09-16', '2022-09-16 00:08:00', 1, 'En este espacio se escribe el motivo de consulta, ExF, y plan de atención del profesional de salud.', 'Acetaminofen TAB 500mg', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id_Paciente` int(11) NOT NULL,
  `tipo_Identificacion` varchar(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(20) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `eapb` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id_Paciente`, `tipo_Identificacion`, `nombre`, `apellidos`, `telefono`, `eapb`, `email`) VALUES
(244354, 'cedula', 'Maria', 'Duarte', '3112345676', 'soat', 'duarte@gmail.com'),
(1110505680, 'cedula', 'Daniela', 'Correa Hernandez', '3105728885', 'Nueva EPS', 'daniela@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`id_Cita`),
  ADD KEY `id_Paciente` (`id_Paciente`);

--
-- Indices de la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD PRIMARY KEY (`id_Consulta`),
  ADD KEY `id_Cita` (`id_Cita`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id_Paciente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `id_Cita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `consulta`
--
ALTER TABLE `consulta`
  MODIFY `id_Consulta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `cita_ibfk_1` FOREIGN KEY (`id_Paciente`) REFERENCES `paciente` (`id_Paciente`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `consulta`
--
ALTER TABLE `consulta`
  ADD CONSTRAINT `consulta_ibfk_1` FOREIGN KEY (`id_Cita`) REFERENCES `cita` (`id_Cita`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
