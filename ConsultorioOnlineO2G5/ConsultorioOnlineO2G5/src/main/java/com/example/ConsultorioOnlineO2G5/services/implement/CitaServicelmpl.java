/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.services.implement;

import com.example.ConsultorioOnlineO2G5.dao.CitaDao;
import com.example.ConsultorioOnlineO2G5.model.Cita;
import com.example.ConsultorioOnlineO2G5.services.CitaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CitaServicelmpl implements CitaService {
     @Autowired
    private CitaDao citaDao;

    @Override
    @Transactional(readOnly = false)
    public Cita save(Cita cita) {
        return citaDao.save(cita);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        citaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Cita findById(Integer id) {
        return citaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Cita> findAll() {
        return (List<Cita>) citaDao.findAll();
    }
    
    
}
