/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "paciente")
public class Paciente implements Serializable {

    @Id
    @Column(name = "id_paciente")
    private Integer id_Paciente;

    @Column(name = "tipo_identificacion")
    private String tipo_Identificacion;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "eapb")
    private String eapb;

    public Integer getId_Paciente() {
        return id_Paciente;
    }

    public void setId_Paciente(Integer id_Paciente) {
        this.id_Paciente = id_Paciente;
    }

    public String getTipo_Identificacion() {
        return tipo_Identificacion;
    }

    public void setTipo_Identificacion(String tipo_Identificacion) {
        this.tipo_Identificacion = tipo_Identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEapb() {
        return eapb;
    }

    public void setEapb(String eapb) {
        this.eapb = eapb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "email")
    private String email;

    

}
