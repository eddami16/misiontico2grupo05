/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.controller;

import com.example.ConsultorioOnlineO2G5.model.Cita;
import com.example.ConsultorioOnlineO2G5.services.CitaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/cita")
public class CitaController {
    
    @Autowired
    private CitaService citaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Cita> agregar(@RequestBody Cita cita) {
        Cita obj = citaservice.save(cita);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Cita> eliminar(@PathVariable Integer id) {
        Cita obj = citaservice.findById(id);
        if (obj != null) {
            citaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Cita> editar(@RequestBody Cita cita) {
        Cita obj = citaservice.findById(cita.getId_Cita());
        if (obj != null) {
            obj.setFecha_Cita(cita.getFecha_Cita());
            obj.setHora_Cita(cita.getHora_Cita());
            obj.setConsultorio(cita.getConsultorio());
            obj.setId_Doctor(cita.getId_Doctor());
            obj.setNombre_Doctor(cita.getNombre_Doctor());
            obj.setId_Paciente (cita.getId_Paciente());
            obj.setReserva (cita.getReserva());
            citaservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Cita> consultarTodo() {
        return citaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Cita consultaPorId(@PathVariable Integer id) {
        return citaservice.findById(id);
    }
   
}
