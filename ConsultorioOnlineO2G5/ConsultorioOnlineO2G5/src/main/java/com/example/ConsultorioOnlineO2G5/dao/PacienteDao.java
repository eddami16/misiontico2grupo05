/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.dao;

import com.example.ConsultorioOnlineO2G5.model.Paciente;
import org.springframework.data.repository.CrudRepository;

public interface PacienteDao extends CrudRepository<Paciente,Integer>{
    
}
