/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.controller;

import com.example.ConsultorioOnlineO2G5.model.Paciente;
import com.example.ConsultorioOnlineO2G5.services.PacienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/paciente")
public class PacienteController {
  @Autowired
    private PacienteService pacienteservice;

    @PostMapping(value = "/")
    public ResponseEntity<Paciente> agregar(@RequestBody Paciente paciente) {
        Paciente obj = pacienteservice.save(paciente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Paciente> eliminar(@PathVariable Integer id) {
        Paciente obj = pacienteservice.findById(id);
        if (obj != null) {
            pacienteservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Paciente> editar(@RequestBody Paciente paciente) {
        Paciente obj = pacienteservice.findById(paciente.getId_Paciente());
        if (obj != null) {
            obj.setTipo_Identificacion(paciente.getTipo_Identificacion());
            obj.setNombre(paciente.getNombre());
            obj.setApellidos(paciente.getApellidos());
            obj.setTelefono(paciente.getTelefono());
            obj.setEapb(paciente.getEapb());
            obj.setEmail(paciente.getEmail());
            pacienteservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Paciente> consultarTodo() {
        return pacienteservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Paciente consultaPorId(@PathVariable Integer id) {
        return pacienteservice.findById(id);
    }
   
    
    
}
