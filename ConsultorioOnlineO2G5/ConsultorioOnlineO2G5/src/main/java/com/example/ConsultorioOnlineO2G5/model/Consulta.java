/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "consulta")
public class Consulta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_consulta")
    private Integer id_Consulta;

    @Column(name = "fecha_cons")
    private String fecha_Cons;

    @Column(name = "hora_cons")
    private String hora_Cons;

    @Column(name = "id_cita")
    private Integer id_Cita;

    @Column(name = "consulta")
    private String consulta;

    @Column(name = "nombre_med")
    private String nombre_Med;

    @Column(name = "cant_med")
    private Integer cant_Med;

    public Integer getId_Consulta() {
        return id_Consulta;
    }

    public void setId_Consulta(Integer id_Consulta) {
        this.id_Consulta = id_Consulta;
    }

    public String getFecha_Cons() {
        return fecha_Cons;
    }

    public void setFecha_Cons(String fecha_Cons) {
        this.fecha_Cons = fecha_Cons;
    }

    public String getHora_Cons() {
        return hora_Cons;
    }

    public void setHora_Cons(String hora_Cons) {
        this.hora_Cons = hora_Cons;
    }

    public Integer getId_Cita() {
        return id_Cita;
    }

    public void setId_Cita(Integer id_Cita) {
        this.id_Cita = id_Cita;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public String getNombre_Med() {
        return nombre_Med;
    }

    public void setNombre_Med(String nombre_Med) {
        this.nombre_Med = nombre_Med;
    }

    public Integer getCant_Med() {
        return cant_Med;
    }

    public void setCant_Med(Integer cant_Med) {
        this.cant_Med = cant_Med;
    }

}
