/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.controller;

import com.example.ConsultorioOnlineO2G5.model.Consulta;
import com.example.ConsultorioOnlineO2G5.services.ConsultaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/consulta")
public class ConsultaController {

    @Autowired
    private ConsultaService consultaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Consulta> agregar(@RequestBody Consulta consulta) {
        Consulta obj = consultaservice.save(consulta);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Consulta> eliminar(@PathVariable Integer id) {
        Consulta obj = consultaservice.findById(id);
        if (obj != null) {
            consultaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Consulta> editar(@RequestBody Consulta consulta) {
        Consulta obj = consultaservice.findById(consulta.getId_Consulta());
        if (obj != null) {

            obj.setFecha_Cons(consulta.getFecha_Cons());
            obj.setHora_Cons(consulta.getHora_Cons());
            obj.setId_Cita(consulta.getId_Cita());
            obj.setConsulta(consulta.getConsulta());
            obj.setNombre_Med(consulta.getNombre_Med());
            obj.setCant_Med(consulta.getCant_Med());

            consultaservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Consulta> consultarTodo() {
        return consultaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Consulta consultaPorId(@PathVariable Integer id) {
        return consultaservice.findById(id);
    }

}
