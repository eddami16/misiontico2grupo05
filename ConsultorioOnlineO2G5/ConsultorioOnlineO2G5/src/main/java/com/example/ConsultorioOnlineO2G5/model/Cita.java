/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "cita")
public class Cita implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cita")
    private Integer id_Cita;

    @Column(name = "fecha_cita")
    private String fecha_Cita;

    @Column(name = "hora_cita")
    private String hora_Cita;

    @Column(name = "consultorio")
    private String consultorio;

    @Column(name = "id_doctor")
    private Integer id_Doctor;

    @Column(name = "nombre_doctor")
    private String nombre_Doctor;

    @Column(name = "id_paciente")
    private Integer id_Paciente;

    @Column(name = "reserva")
    private String reserva;

    public Integer getId_Cita() {
        return id_Cita;
    }

    public void setId_Cita(Integer id_Cita) {
        this.id_Cita = id_Cita;
    }

    public String getFecha_Cita() {
        return fecha_Cita;
    }

    public void setFecha_Cita(String fecha_Cita) {
        this.fecha_Cita = fecha_Cita;
    }

    public String getHora_Cita() {
        return hora_Cita;
    }

    public void setHora_Cita(String hora_Cita) {
        this.hora_Cita = hora_Cita;
    }

    public String getConsultorio() {
        return consultorio;
    }

    public void setConsultorio(String consultorio) {
        this.consultorio = consultorio;
    }

    public Integer getId_Doctor() {
        return id_Doctor;
    }

    public void setId_Doctor(Integer id_Doctor) {
        this.id_Doctor = id_Doctor;
    }

    public String getNombre_Doctor() {
        return nombre_Doctor;
    }

    public void setNombre_Doctor(String nombre_Doctor) {
        this.nombre_Doctor = nombre_Doctor;
    }

    public Integer getId_Paciente() {
        return id_Paciente;
    }

    public void setId_Paciente(Integer id_Paciente) {
        this.id_Paciente = id_Paciente;
    }

    public String getReserva() {
        return reserva;
    }

    public void setReserva(String reserva) {
        this.reserva = reserva;
    }

    
}
