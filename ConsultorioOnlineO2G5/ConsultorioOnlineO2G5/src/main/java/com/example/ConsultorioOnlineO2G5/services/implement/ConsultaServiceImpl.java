/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.services.implement;

import com.example.ConsultorioOnlineO2G5.dao.ConsultaDao;
import com.example.ConsultorioOnlineO2G5.model.Consulta;
import com.example.ConsultorioOnlineO2G5.services.ConsultaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConsultaServiceImpl implements ConsultaService {
     @Autowired
    private ConsultaDao consultaDao;

    @Override
    @Transactional(readOnly = false)
    public Consulta save(Consulta consulta) {
        return consultaDao.save(consulta);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        consultaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Consulta findById(Integer id) {
        return consultaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Consulta> findAll() {
        return (List<Consulta>) consultaDao.findAll();
    }
    
}
