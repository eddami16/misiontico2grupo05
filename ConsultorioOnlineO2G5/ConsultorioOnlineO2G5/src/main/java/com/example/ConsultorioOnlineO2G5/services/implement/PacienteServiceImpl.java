/**
 O2 Grupo 05: 
2221647 – Daniela Correa Hernandez – Product owner
2220848 – Edda Myle Urdaneta Cordoba – Scrum Master
2224597 – Campo Elias Leal Torres – Desarrollador
2221388 – Joana Sabrina Garcia Castro – Desarrollador
2221912 – Jhon Alexander Monroy Benitez – Desarrollador
 */
package com.example.ConsultorioOnlineO2G5.services.implement;
import com.example.ConsultorioOnlineO2G5.dao.PacienteDao;
import com.example.ConsultorioOnlineO2G5.model.Paciente;
import com.example.ConsultorioOnlineO2G5.services.PacienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PacienteServiceImpl implements PacienteService {
    
     @Autowired
    private PacienteDao pacienteDao;

    @Override
    @Transactional(readOnly = false)
    public Paciente save(Paciente paciente) {
        return pacienteDao.save(paciente);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        pacienteDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Paciente findById(Integer id) {
        return pacienteDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Paciente> findAll() {
        return (List<Paciente>) pacienteDao.findAll();
    }
    
    
}
