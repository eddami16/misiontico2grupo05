package com.example.ConsultorioOnlineO2G5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioOnlineO2G5Application {

	public static void main(String[] args) {
		SpringApplication.run(ConsultorioOnlineO2G5Application.class, args);
	}

}
